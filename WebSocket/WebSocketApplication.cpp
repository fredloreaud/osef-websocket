#include "WebSocketApplication.h"
#include "Debug.h"

OSEF::WebSocketApplication::WebSocketApplication() {}

OSEF::WebSocketApplication::~WebSocketApplication() {}

bool OSEF::WebSocketApplication::serve(TcpServer& server)
{
    OSEF::WebSocketServerSession session(&server);

    bool ret = serve(session);

    if(session.isClosed())
    {
        ret = true;
    }
    else
    {
        _DWARN("couldn't close session");
    }

    return ret;
}
