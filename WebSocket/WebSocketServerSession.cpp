#include "WebSocketServerSession.h"
#include "WebSocketUpgradeFrame.h"
#include "TimeOut.h"
#include "Debug.h"

OSEF::WebSocketServerSession::WebSocketServerSession(OSEF::TcpServer* ep)
    : WebSocketSession(ep)
    , resourceId(""){}

OSEF::WebSocketServerSession::~WebSocketServerSession(){}

bool OSEF::WebSocketServerSession::setUp()
{
    if(!upgraded)
    {
        OSEF::WebSocketUpgradeFrame buffer;
        if(receiveDirectBuffer(buffer))
        {
            if(buffer.extractRequest(resourceId))
            {
                _DOUT("WebSocket upgrade uri " << resourceId);

                if(buffer.reset())
                {
                    if(buffer.pushResponse())
                    {
                        if(sendDirectBuffer(buffer))
                        {
                            // to let client receive upgrade before sending data ...
                            // might better to correct in client ...
                            if(!OSEF::sleepms(100))
                            {
                                _DERR("error sleeping after upgrade");
                            }
                            upgraded = true;
                        }
                    }
                }
            }
        }
        else
        {
            OSEF::WebSocketUpgradeFrame responseBuffer;
            if(responseBuffer.pushBadRequestError())
            {
                if(sendDirectBuffer(buffer))
                {
                    _DERR("error sending bad request error");
                }
            }
        }
    }

    return upgraded;
}

bool OSEF::WebSocketServerSession::sendPayload(const std::string& payload)
{
    OSEF::WebSocketNetBuffer buffer;
    const bool ret = sendPayloadBuffer(buffer, payload, false);
    return ret;
}

bool OSEF::WebSocketServerSession::receivePayload(std::string& payload)
{
    bool ret = false;

    OSEF::WebSocketNetBuffer buffer;
    if(receiveBuffer(buffer))
    {
        ret = buffer.extractPayloadCharacters(payload, true);
    }

    return ret;
}
