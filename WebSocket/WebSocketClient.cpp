#include "WebSocketClient.h"
#include "Debug.h"

OSEF::WebSocketClient::WebSocketClient(const std::string& h, const std::string& p, const std::string& u)
    : client(h, p)
    , session(&client, h, u){}

OSEF::WebSocketClient::~WebSocketClient()
{
    if(!session.tearDown())
    {
        _DERR("error tearing down session");
    }
}

bool OSEF::WebSocketClient::sendPayload(const std::string& payload)
{
    const bool ret = session.sendPayload(payload);
    return ret;
}

bool OSEF::WebSocketClient::receivePayload(std::string& payload)
{
    const bool ret = session.receivePayload(payload);
    return ret;
}

bool OSEF::WebSocketClient::receivePayload(std::string& payload, const timespec& to)
{
    const bool ret = session.receivePayload(payload, to);
    return ret;
}
