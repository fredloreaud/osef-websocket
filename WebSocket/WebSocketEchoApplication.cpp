#include "WebSocketEchoApplication.h"
#include "WebSocketServerSession.h"
#include "WebSocketNetBuffer.h"

#include <string>

OSEF::WebSocketEchoApplication::WebSocketEchoApplication()
    :WebSocketApplication(){}

OSEF::WebSocketEchoApplication::~WebSocketEchoApplication(){}

bool OSEF::WebSocketEchoApplication::serve(OSEF::WebSocketServerSession& session)
{
    bool ret = false;

    std::string payload = "";
    do
    {
        ret = false;
        if(session.receivePayload(payload))
        {
            ret = session.sendPayload(payload);
        }
    }while(ret);

    return ret;
}
