#ifndef WEBSOCKETECHOAPPLICATION_H
#define WEBSOCKETECHOAPPLICATION_H

#include "WebSocketApplication.h"

namespace OSEF { class WebSocketEchoApplication : public WebSocketApplication {
public:
    WebSocketEchoApplication();
    virtual ~WebSocketEchoApplication();

protected:
    bool serve(OSEF::WebSocketServerSession& session) override;

private:
    WebSocketEchoApplication(const WebSocketEchoApplication& orig);
    WebSocketEchoApplication& operator=(const WebSocketEchoApplication& orig);
};
}  // namespace OSEF

#endif /* WEBSOCKETECHOAPPLICATION_H */

