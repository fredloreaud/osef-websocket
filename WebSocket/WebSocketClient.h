#ifndef DCWEBSOCKETCLIENT_H
#define DCWEBSOCKETCLIENT_H

#include "TcpClient.h"
#include "WebSocketClientSession.h"

#include <string>

namespace OSEF { class WebSocketClient {
public:
    WebSocketClient(const std::string& h, const std::string& p, const std::string& u);
    virtual ~WebSocketClient();

    bool sendPayload(const std::string& payload);
    bool receivePayload(std::string& payload);
    bool receivePayload(std::string& payload, const timespec& to);

private:
    WebSocketClient(const WebSocketClient& orig);
    WebSocketClient& operator=(const WebSocketClient& orig);

    OSEF::TcpClient client;
    WebSocketClientSession session;
};
}  // namespace OSEF

#endif /* DCWEBSOCKETCLIENT_H */

