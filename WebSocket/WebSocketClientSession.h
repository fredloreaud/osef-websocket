#ifndef WEBSOCKETCLIENTSESSION_H
#define WEBSOCKETCLIENTSESSION_H

#include "WebSocketSession.h"
#include "TcpClient.h"

#include <string>

namespace OSEF { class WebSocketClientSession : public WebSocketSession {
public:
    WebSocketClientSession(OSEF::TcpClient* ep, const std::string& h, const std::string& uri);
    virtual ~WebSocketClientSession();

    bool sendPayload(const std::string& payload);
    bool receivePayload(std::string& payload);
    bool receivePayload(std::string& payload, const timespec& to);

private:
    WebSocketClientSession(const WebSocketClientSession& orig);
    WebSocketClientSession& operator=(const WebSocketClientSession& orig);

    bool setUp() override;

    std::string host;
    std::string resourceId;
};
}  // namespace OSEF

#endif /* WEBSOCKETCLIENTSESSION_H */

