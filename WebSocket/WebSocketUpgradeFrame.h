#ifndef OSEFWEBSOCKETUPGRADEFRAME_H
#define OSEFWEBSOCKETUPGRADEFRAME_H

#include "HttpFrame.h"

#include <string>

namespace OSEF {
    const std::string MagicString = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

    const std::string HttpSwitchingProtocols = HttpVersion + " 101 Switching Protocols";

    const std::string WSU_Host = "HOST";
    const HF_Pair WSU_Connection = {"CONNECTION", "Upgrade"};
    const HF_Pair WSU_Upgrade = {"UPGRADE", "websocket"};
    const HF_Pair WSU_Version = {"SEC-WEBSOCKET-VERSION", "13"};
    const std::string WSU_Key = "SEC-WEBSOCKET-KEY";
    const std::string WSU_Accept = "SEC-WEBSOCKET-ACCEPT";
}  // namespace OSEF

namespace OSEF { class WebSocketUpgradeFrame : public HttpFrame  {
public:
    WebSocketUpgradeFrame();
    virtual ~WebSocketUpgradeFrame();

    bool pushRequest(const std::string& ip = "127.0.0.1", const std::string& uri = "/");
    bool extractResponse();

    bool extractRequest(std::string& uri);
    bool pushResponse();
    bool pushBadRequestError();

private:
    WebSocketUpgradeFrame(const WebSocketUpgradeFrame& orig);
    WebSocketUpgradeFrame& operator=(const WebSocketUpgradeFrame& orig);

    std::string getSecurityAccept();

    std::string keyBase64;
};
}  // namespace OSEF

#endif /* OSEFWEBSOCKETUPGRADEFRAME_H */

