#include "WebSocketClientSession.h"
#include "WebSocketUpgradeFrame.h"
#include "Debug.h"

OSEF::WebSocketClientSession::WebSocketClientSession(OSEF::TcpClient* ep, const std::string& h, const std::string& uri)
    : WebSocketSession(ep)
    , host(h)
    , resourceId(uri){}

OSEF::WebSocketClientSession::~WebSocketClientSession(){}

bool OSEF::WebSocketClientSession::setUp()
{
    if(!upgraded)
    {
        WebSocketUpgradeFrame buffer;
        if(buffer.pushRequest(host, resourceId))
        {
            if(sendDirectBuffer(buffer))
            {
                if(receiveDirectBuffer(buffer))
                {
                    upgraded = buffer.extractResponse();
                }
            }
        }
    }

    return upgraded;
}

bool OSEF::WebSocketClientSession::sendPayload(const std::string& payload)
{
    OSEF::WebSocketNetBuffer buffer;
    const bool ret = sendPayloadBuffer(buffer, payload, true);
    return ret;
}

bool OSEF::WebSocketClientSession::receivePayload(std::string& payload)
{
    bool ret = false;

    OSEF::WebSocketNetBuffer buffer;
    if(receiveBuffer(buffer))
    {
        ret = buffer.extractPayloadCharacters(payload, false);
    }

    return ret;
}

bool OSEF::WebSocketClientSession::receivePayload(std::string& payload, const timespec& to)
{
    bool ret = false;

    OSEF::WebSocketNetBuffer buffer;
    if(receiveBuffer(buffer, to))
    {
        ret = buffer.extractPayloadCharacters(payload, false);
    }

    return ret;
}
