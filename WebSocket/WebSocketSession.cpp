#include "WebSocketSession.h"
#include "WebSocketNetBuffer.h"
#include "Debug.h"

OSEF::WebSocketSession::WebSocketSession(OSEF::NetworkEndpoint* ep)
    : NetBufferSession(ep)
    , upgraded(false)
    , closeHandshakeReceived(false){}

OSEF::WebSocketSession::~WebSocketSession() {}

bool OSEF::WebSocketSession::tearDown()
{
    bool ret = true;

    if(upgraded && !closeHandshakeReceived)
    {
        ret = sendCloseHandshake();
    }

    return ret;
}

bool OSEF::WebSocketSession::sendCloseHandshake()
{
    bool ret = false;

    WebSocketNetBuffer buffer;
    if(buffer.setCloseHeader())
    {
        ret = sendDirectBuffer(buffer);
        _DOUT("sent close handshake");
    }

    return ret;
}

bool OSEF::WebSocketSession::validate(const std::string& s)
{
    bool ret = false;

    if(s.size() >= 1)
    {
        if(!WebSocketNetBuffer::isCloseHeader(s.at(0)))
        {
            ret = true;
        }
        else
        {
            _DOUT("received close handshake");
            closeHandshakeReceived = true;
            upgraded = false;
            ret = false;
        }
    }
    else
    {
        _DERR("buffer empty");
    }

    return ret;
}

bool OSEF::WebSocketSession::sendPayloadBuffer(WebSocketNetBuffer& buffer, const std::string& payload, const bool& masked)
{
    bool ret = false;

    if(buffer.pushPayloadCharacters(payload, masked))
    {
        ret = sendBuffer(buffer);
    }

    return ret;
}
