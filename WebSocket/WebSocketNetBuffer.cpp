#include "WebSocketNetBuffer.h"
#include "Randomizer.h"
#include "Debug.h"

/// 1460 = Ethernet v2 max MTU 1500 bytes - IP header (20 bytes) - TCP header (20bytes)
/// 1 = size of WebSocketHeader
OSEF::WebSocketNetBuffer::WebSocketNetBuffer()
    :NetBuffer(1460, 1)
{
}

OSEF::WebSocketNetBuffer::~WebSocketNetBuffer()
{
}

bool OSEF::WebSocketNetBuffer::setHeader(const WebSocketHeader& h)
{
    const bool ret = setUInt8(h.data, 0);
    return ret;
}

bool OSEF::WebSocketNetBuffer::setCloseHeader()
{
    WebSocketHeader h = {WS_OP_CLOSE, 0, WS_FINAL_FRAG_YES};
    const bool ret = setHeader(h);
    return ret;
}

bool OSEF::WebSocketNetBuffer::getHeader(WebSocketHeader& h)
{
    const bool ret = getUInt8(h.data, 0);
    return ret;
}

bool OSEF::WebSocketNetBuffer::isCloseHeader(const uint8_t& b)
{
    bool ret = false;

    WebSocketHeader h;
    h.data = b;

    if(h.fields.finalfrag)
    {
        if(h.fields.opcode == WS_OP_CLOSE)
        {
            ret = true;
        }
    }
    else
    {
        _DERR("multi fragments websocket not supported yet");
    }

    return ret;
}

bool OSEF::WebSocketNetBuffer::pushPayloadLength(const uint64_t& l, const bool& masked)
{
    bool ret = false;

    // single frame limitation
    if(l <= 1458UL)  // 1460 - header (1 byte) - payload length (1 byte)
    {
        // set mask bit
        uint8_t maskbit = 0x00;
        if(masked)
        {
            maskbit = 0x80;
        }

        if(l < 126UL)
        {
            ret = pushUInt8(l|maskbit);  // push 7 bits payload length
        }
        else
        {
            // limited to l < 0x10000
            if(pushUInt8(126|maskbit))  // push marker for 16 bits payload length
            {
                ret = pushUInt16(l);  // push 16 bits payload length
            }

            // marker for 64 bits payload length is 127|maskbit
            // useless until single frame limitation
        }
    }
    else
    {
        _DERR("single frame limitation");
    }

    return ret;
}

bool OSEF::WebSocketNetBuffer::extractPayloadLength(uint64_t& l, const bool& masked)
{
    bool ret = false;

    uint8_t length8 = 0;
    if(extractUInt8(length8))
    {
        if(masked == ((length8&0x80) > 0))
        {
            length8 &= ~0x80;
            uint16_t length16 = 0;
            switch(length8)
            {
                case 126:
                    ret = extractUInt16(length16);
                    l = length16;
                    break;
                case 127:
                    ret = extractUInt64(l);
                    break;
                default:
                    l = length8;
                    ret = true;
                    break;
            }
        }
        else
        {
            if(masked)
            {
                _DERR("data from client should be masked");
            }
            else
            {
                _DERR("data from server should not be masked");
            }
        }
    }
    return ret;
}

void OSEF::WebSocketNetBuffer::maskString(const uint32_t& m, std::string& s)
{
    for(size_t i = 0; i < s.size(); i++)
    {
        s[i] ^= (m>>((3-(i%4))*8))&0xff;
    }
}

void OSEF::WebSocketNetBuffer::maskUint32(const uint32_t& m, uint32_t& d)
{
    d ^= m;
}

// test mask 0x37 0xfa 0x21 0x3d
// test masked 0x7f 0x9f 0x4d 0x51 0x58
// test unmasked 0x48 0x65 0x6c 0x6c 0x6f ("Hello")
bool OSEF::WebSocketNetBuffer::testMask()
{
    bool ret = false;

    uint32_t m = 0x3d21fa37;
    std::string s = "Hello";
    maskString(m, s);

    std::string expected = "";
    expected.resize(5);
    expected[0] = 0x7f;
    expected[1] = 0x9f;
    expected[2] = 0x4d;
    expected[3] = 0x51;
    expected[4] = 0x58;

    if(s == expected)
    {
        ret = true;
    }

    return ret;
}

// bool OSEF::WebSocketNetBuffer::pushPayloadUInt32(const uint32_t& d, const bool& masked)
//{
//    bool ret = false;
//
//    if(resetForSetup())
//    {
//        WebSocketHeader h = {WS_OP_BIN, 0, WS_FINAL_FRAG_YES};
//        if(setHeader(h))
//        {
//            if(pushPayloadLength(sizeof(d), masked))
//            {
//                if(!masked)
//                {
//                    ret = pushUInt32(d);
//                }
//                else
//                {
//                    uint32_t mask = OSEF::Randomizer().getInt32();
//                    if(pushUInt32(mask))
//                    {
//                        uint32_t md = d;
//                        maskUint32(mask, md);
//                        ret = pushUInt32(md);
//                    }
//                }
//            }
//        }
//    }
//
//    return ret;
//}


bool OSEF::WebSocketNetBuffer::pushPayloadCharacters(const std::string& s, const bool& masked)
{
    bool ret = false;

    if(resetForSetup())
    {
        WebSocketHeader h = {WS_OP_TEXT, 0, WS_FINAL_FRAG_YES};
        if(setHeader(h))
        {
            if(pushPayloadLength(s.size(), masked))
            {
                if(!masked)
                {
                    ret = pushCharacters(s);
                }
                else
                {
                    uint32_t mask = OSEF::Randomizer().getInt32();
                    if(pushUInt32(mask))
                    {
                        std::string ms = s;
                        maskString(mask, ms);
                        ret = pushCharacters(ms);
                    }
                }
            }
        }
    }

    return ret;
}

// bool OSEF::WebSocketNetBuffer::extractPayloadUInt32(uint32_t& d, const bool& masked)
//{
//    bool ret = false;
//
//    WebSocketHeader h;
//    if(getHeader(h))
//    {
//        if(h.fields.finalfrag)
//        {
//            if(h.fields.opcode==WS_OP_TEXT)
//            {
//                uint64_t l = 0;
//                if(extractPayloadLength(l, masked))
//                {
//                    if(!masked)
//                    {
//                        if(l==sizeof(d))
//                        {
//                            ret = extractUInt32(d);
//                        }
//                        else
//                        {
//                            _DERR("wrong length (found "<<l<<" instead of "<<sizeof(d)<<")");
//                        }
//                    }
//                    else
//                    {
//                        uint32_t m = 0;
//                        if(extractUInt32(m))
//                        {
//                            if(l==sizeof(d))
//                            {
//                                if(extractUInt32(d))
//                                {
//                                    maskUint32(m, d);
//                                    ret = true;
//                                }
//                            }
//                            else
//                            {
//                                _DERR("wrong length (found "<<l<<" instead of "<<sizeof(d)<<")");
//                            }
//                        }
//                    }
//                }
//            }
//            else
//            {
//                _DERR("wrong opcode (found "<<+h.fields.opcode<<" instead of "<<+WS_OP_TEXT<<")");
//            }
//        }
//        else
//        {
//            _DERR("multi fragments websocket not supported yet");
//        }
//    }
//    return ret;
//}

bool OSEF::WebSocketNetBuffer::extractPayloadCharacters(std::string& s, const bool& masked)
{
    bool ret = false;

    WebSocketHeader h;
    if(getHeader(h))
    {
        if(h.fields.finalfrag)
        {
            if(h.fields.opcode == WS_OP_TEXT)
            {
                uint64_t l = 0;
                if(extractPayloadLength(l, masked))
                {
                    if(!masked)
                    {
                        ret = extractCharacters(s, l);
                    }
                    else
                    {
                        uint32_t m = 0;
                        if(extractUInt32(m))
                        {
                            s = "";
                            if(extractCharacters(s, l))
                            {
                                maskString(m, s);
                                ret = true;
                            }
                        }
                    }
                }
            }
            else
            {
                _DERR("wrong opcode (found " << +h.fields.opcode << " instead of " << +WS_OP_TEXT << ")");
            }
        }
        else
        {
            _DERR("multi fragments websocket not supported yet");
        }
    }
    return ret;
}
