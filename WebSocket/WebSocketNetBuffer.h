#ifndef OSEFWEBSOCKETNETBUFFER_H
#define OSEFWEBSOCKETNETBUFFER_H

#include <string>

#include "NetBuffer.h"

namespace OSEF {

union WebSocketHeader
{
    struct {
        uint8_t opcode:4;
        uint8_t reserved:3;
        uint8_t finalfrag:1;
    } __attribute__((packed)) fields;

    uint8_t data;
};

const bool WS_FINAL_FRAG_YES = true;

const uint8_t WS_OP_CONT = 0x00;
const uint8_t WS_OP_TEXT = 0x01;
const uint8_t WS_OP_BIN = 0x02;
const uint8_t WS_OP_CLOSE = 0x08;
const uint8_t WS_OP_PING = 0x09;
const uint8_t WS_OP_PONG = 0x0a;

class WebSocketNetBuffer : public OSEF::NetBuffer
{
public:
    WebSocketNetBuffer();
    virtual ~WebSocketNetBuffer();

    bool setCloseHeader();
    static bool isCloseHeader(const uint8_t& b);

//    bool pushPayloadUInt32(const uint32_t& d, const bool& masked);
    bool pushPayloadCharacters(const std::string& s, const bool& masked);

//    bool extractPayloadUInt32(uint32_t& d, const bool& masked);
    bool extractPayloadCharacters(std::string& s, const bool& masked);

protected:
    bool testMask();

protected:
    bool getHeader(WebSocketHeader& h);
    bool setHeader(const WebSocketHeader& h);

    bool pushPayloadLength(const uint64_t& l, const bool& masked);
    bool extractPayloadLength(uint64_t& l, const bool& masked);

    void maskString(const uint32_t& m, std::string& s);
    void maskUint32(const uint32_t& m, uint32_t& d);

private:
    WebSocketNetBuffer(const WebSocketNetBuffer& orig);
    WebSocketNetBuffer& operator=(const WebSocketNetBuffer& orig);
};
}  // namespace OSEF

#endif /* OSEFWEBSOCKETNETBUFFER_H */

