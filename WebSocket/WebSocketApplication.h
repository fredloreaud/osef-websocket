#ifndef OSEFWEBSOCKETAPPLICATION_H
#define OSEFWEBSOCKETAPPLICATION_H

#include "TcpApplication.h"
#include "WebSocketServerSession.h"
#include "WebSocketNetBuffer.h"

namespace OSEF { class WebSocketApplication : public TcpApplication {
public:
    WebSocketApplication();
    virtual ~WebSocketApplication();

    bool serve(TcpServer& server) override;

protected:
    virtual bool serve(OSEF::WebSocketServerSession& session) = 0;

private:
    WebSocketApplication(const WebSocketApplication& orig);
    WebSocketApplication& operator=(const WebSocketApplication& orig);
};
}  // namespace OSEF

#endif /* OSEFWEBSOCKETAPPLICATION_H */

