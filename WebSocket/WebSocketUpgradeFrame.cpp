#include "WebSocketUpgradeFrame.h"
#include <openssl/sha.h>  // SHA1
#include "Randomizer.h"
#include "Debug.h"

OSEF::WebSocketUpgradeFrame::WebSocketUpgradeFrame()
    : HttpFrame()
    , keyBase64("")
{
    uint8_t bytes[16];
    OSEF::Randomizer().get16UInt8(bytes);

    /// https://tools.ietf.org/html/rfc4648#section-4
    encodeToBase64(&bytes[0], 16, keyBase64, 24);
}

OSEF::WebSocketUpgradeFrame::~WebSocketUpgradeFrame(){}

bool OSEF::WebSocketUpgradeFrame::pushRequest(const std::string& ip, const std::string& uri)
{
    const HF_Container fields
    ({
            {WSU_Host, ip}
        ,   WSU_Connection
        ,   WSU_Upgrade
        ,   WSU_Version
        ,   {WSU_Key, keyBase64}
    });

    const bool ret = pushHeader(HttpGet, uri, fields);
    return ret;
}

bool OSEF::WebSocketUpgradeFrame::extractResponse()
{
    bool ret = false;

    if(extractHeaderStartLine(HttpSwitchingProtocols))
    {
        if(isFieldEqual(WSU_Connection))
        {
            if(isFieldEqual(WSU_Upgrade))
            {
                ret = isFieldEqual(WSU_Accept, getSecurityAccept());
            }
        }
    }

    return ret;
}

bool OSEF::WebSocketUpgradeFrame::extractRequest(std::string& uri)
{
    bool ret = false;

    if(extractHeaderUri(HttpGet, uri))
    {
        if(isFieldEqual(WSU_Connection))
        {
            if(isFieldEqual(WSU_Upgrade))
            {
                if(isFieldEqual(WSU_Version))
                {
                    ret = getField(WSU_Key, keyBase64);
                }
            }
        }
    }

    return ret;
}

bool OSEF::WebSocketUpgradeFrame::pushResponse()
{
    const HF_Container fields
    ({
            WSU_Connection
        ,   WSU_Upgrade
        ,   {WSU_Accept, getSecurityAccept()}
    });

    const bool ret = pushHeader(HttpSwitchingProtocols, fields);
    return ret;
}

bool OSEF::WebSocketUpgradeFrame::pushBadRequestError()
{
    const bool ret = pushHeader(HttpBadRequest);
    return ret;
}

std::string OSEF::WebSocketUpgradeFrame::getSecurityAccept()
{
    std::string magicKey = keyBase64 + MagicString;

    uint8_t hash[20];
    SHA1((const unsigned char*)magicKey.c_str(), magicKey.size(), &hash[0]);

    std::string ret = "";
    encodeToBase64(&hash[0], 20, ret, 28);

    return ret;
}
