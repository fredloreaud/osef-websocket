#ifndef WEBSOCKETSERVERSESSION_H
#define WEBSOCKETSERVERSESSION_H

#include "WebSocketSession.h"
#include "TcpServer.h"

#include <string>

namespace OSEF { class WebSocketServerSession : public WebSocketSession {
public:
    explicit WebSocketServerSession(OSEF::TcpServer* ep);
    virtual ~WebSocketServerSession();

    bool sendPayload(const std::string& payload);
    bool receivePayload(std::string& payload);

private:
    WebSocketServerSession(const WebSocketServerSession& orig);
    WebSocketServerSession& operator=(const WebSocketServerSession& orig);

    bool setUp() override;

    std::string resourceId;
};
}  // namespace OSEF

#endif /* WEBSOCKETSERVERSESSION_H */

