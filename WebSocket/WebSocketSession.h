#ifndef DCWEBSOCKETSESSION_H
#define DCWEBSOCKETSESSION_H

#include "NetBufferSession.h"
#include "StreamEndpoint.h"
#include "WebSocketNetBuffer.h"

#include <string>

namespace OSEF { class WebSocketSession : public OSEF::NetBufferSession {
public:
    explicit WebSocketSession(OSEF::NetworkEndpoint* ep);
    virtual ~WebSocketSession();

    bool tearDown() override;

    bool isClosed() const {return closeHandshakeReceived;}

protected:
    bool validate(const std::string& s) override;

    bool sendPayloadBuffer(WebSocketNetBuffer& buffer, const std::string& payload, const bool& masked);

    bool upgraded;

private:
    WebSocketSession(const WebSocketSession& orig);
    WebSocketSession& operator=(const WebSocketSession& orig);

    bool sendCloseHandshake();

    bool closeHandshakeReceived;  // to mark connection was gracefully closes
};
}  // namespace OSEF

#endif /* DCWEBSOCKETSESSION_H */

