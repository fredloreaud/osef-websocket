#include "TcpListenerThread.h"
#include "WebSocketEchoApplication.h"
#include "SignalHandler.h"
#include "TimeOut.h"

#include "getopt.h"

typedef struct
{
    int32_t argc;
    char** argv;
}Arguments;

bool getHostService(const Arguments& arg, std::string& host, std::string& port)
{
    bool ret = true;
    int32_t opt = -1;

    if(arg.argc>1)
    {
	/*use function getopt to get the arguments with option."hu:p:s:v" indicate
	that option h,v are the options without arguments while u,p,s are the
	options with arguments*/
	do
	{
            opt=getopt(arg.argc,arg.argv,"i:p:");
            if(opt!=-1)
            {
                switch(opt)
                {
                    case 'i':
                        host = optarg;
                        break;
                    case 'p':
                        port = optarg;
                        break;
                    default:
                        std::cout<<"Usage:   "<<arg.argv[0UL]<<" [-option] [argument]"<<std::endl;
                        std::cout<<"option:  "<<std::endl;
                        std::cout<<"         "<<"-i  Ethernet IP host (localhost/127.0.0.1)"<<std::endl;
                        std::cout<<"         "<<"-p  Service port"<<std::endl;
                        ret = false;
                        break;
                }
            }
	}while(opt!=-1);
    }

    return ret;
}

int main(int argc, char** argv)
{
    int ret = -1;

    OSEF::SignalHandler signal(SIGINT, SIGTERM);

    std::string host = "localhost";
    std::string port = "8080";

    if(getHostService({argc, argv}, host, port))
    {
        OSEF::WebSocketEchoApplication app;
        OSEF::TcpListenerThread server(host, port, 2, &app);
        if(server.spawnListenRoutine())
        {
            bool loop = false;
            do
            {
                loop = false;
                if(OSEF::sleepms(100))
                {
                    loop = true;
                }
            }while(loop && (!signal.signalReceived()));
            ret = 0;
        }
    }

    return ret;
}
