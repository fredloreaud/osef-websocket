#include "SecureWebSocketServer.h"
#include "Debug.h"

DC::SecureWebSocketServer::SecureWebSocketServer(const int32_t& sock, const mbedtls_ssl_config& config, WebSocketServerApplication& app)
    :server(sock, config, &session)
    ,application(&app)
    ,session(&server){}

DC::SecureWebSocketServer::~SecureWebSocketServer()
{
    if(!session.tearDown())
    {
        _DERR("error tearing down session");
    }
}

bool DC::SecureWebSocketServer::serveURI()
{
    bool ret = false;
    
    if(application!=nullptr)
    {
        ret = application->serveURI(server);
    }
    
    return ret;
}
