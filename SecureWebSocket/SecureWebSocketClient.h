#ifndef DCSECUREWEBSOCKETCLIENT_H
#define DCSECUREWEBSOCKETCLIENT_H

#include "TLSClient.h"
#include "WebSocketClientSession.h"

namespace DC { class SecureWebSocketClient {
public:
    SecureWebSocketClient(const std::string& h, const std::string& p, const std::string& u, const std::string& pem, const std::string& seed="");
    virtual ~SecureWebSocketClient();
    
    bool sendPayload(const std::string& payload);
    bool receivePayload(std::string& payload);
    bool receivePayload(std::string& payload, const timespec& to);
    
private:
    SecureWebSocketClient(const SecureWebSocketClient& orig);
    SecureWebSocketClient& operator=(const SecureWebSocketClient& orig);

    OSEF::TLSClient client;
    WebSocketClientSession session;

};}

#endif /* DCSECUREWEBSOCKETCLIENT_H */

