#include "SecureWebSocketServerThread.h"
#include "MsgQFlags.h" // SF_MQ_RDONLY SF_MQ_CREAT
#include "TimeOut.h" // T50ms
#include "Debug.h"

DC::SecureWebSocketServerThread::SecureWebSocketServerThread(
        const size_t& max
        , const std::string& h
        , const std::string& p
        , WebSocketServerApplication& app
        , const std::string& mqname
        , const std::string& srvCert
        , const std::string& srvKey
        , const std::string& caCert
        , const std::string& seed)
    :listener(h, p, srvCert, srvKey, caCert, seed)
    ,threader(max, mqname)
    ,application(&app){};

DC::SecureWebSocketServerThread::~SecureWebSocketServerThread(){}

bool DC::SecureWebSocketServerThread::listen()
{
    bool ret = false;

    SecureWebSocketListenArg targ;
    
    targ.serverThread = this;
    
    ret = threader.spawnJoinedThread(listenRoutine, &targ, sizeof(targ));
    
    return ret;
}

void* DC::SecureWebSocketServerThread::listenRoutine(void* const targ)
{
    SecureWebSocketListenArg* const arg = static_cast <SecureWebSocketListenArg* const>(targ);
    if(arg!=nullptr)
    {
        SecureWebSocketServerThread* serverThread = arg->serverThread;
        if(serverThread!=nullptr)
        {
            int32_t sock = -1;
            do
            {
                if(serverThread->accept(sock))
                {
                    if(!serverThread->serve(sock))
                    {
                        OSEF::SocketToolbox::closeSocket(sock);
                    }
                }
            }while(sock!=-1);
        }
        else
        {
            _DERR("listener null");
        }
    }
    else
    {
        _DERR("argument null");
    }
   
    return nullptr;
}

bool DC::SecureWebSocketServerThread::accept(int32_t& sock)
{
    const bool ret = listener.acceptClient(sock);    
    return ret;
}

bool DC::SecureWebSocketServerThread::serve(const int32_t& sock)
{
    bool ret = false;

    SecureWebSocketServeArg targ;
    
    targ.sockFD = sock;
    targ.application = application;
    targ.configuration = listener.getConfiguration();

    ret = threader.spawnSubscribedThread(serveRoutine, &targ, sizeof(targ));
    
    return ret;
}

void* DC::SecureWebSocketServerThread::serveRoutine(void* const targ)
{
    SecureWebSocketServeArg* const arg = static_cast <SecureWebSocketServeArg* const>(targ);
    if(arg!=nullptr)
    {
        if(arg->application!=nullptr)
        {            
            SecureWebSocketServer server(arg->sockFD, arg->configuration, *(arg->application));
            if(!server.serveURI())
            {
                if(!server.isSessionClosed())
                {
                    _DERR("URI serving error");
                }
            }
        }
        else
        {
            _DERR("application null");
        }
    }
    else
    {
        _DERR("argument null");
    }
    
    return nullptr;
}
