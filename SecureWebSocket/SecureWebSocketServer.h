#ifndef DCSECUREWEBSOCKETSERVER_H
#define DCSECUREWEBSOCKETSERVER_H

#include "TLSServer.h"
#include "WebSocketServerApplication.h"
#include "WebSocketServerSession.h"

namespace DC { class SecureWebSocketServer {
public:
    SecureWebSocketServer(const int32_t& sock, const mbedtls_ssl_config& config, WebSocketServerApplication& app);
    virtual ~SecureWebSocketServer();
    
    bool isSessionClosed() const {return session.isClosed();}
    
    bool serveURI();
    
private:
    SecureWebSocketServer(const SecureWebSocketServer& orig);
    SecureWebSocketServer& operator=(const SecureWebSocketServer& orig);
 
    OSEF::TLSServer server;
    WebSocketServerApplication* application;
    WebSocketServerSession session;

};}

#endif /* DCSECUREWEBSOCKETSERVER_H */

