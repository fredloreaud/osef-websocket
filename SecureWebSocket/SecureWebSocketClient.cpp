#include "SecureWebSocketClient.h"
#include "Debug.h"

DC::SecureWebSocketClient::SecureWebSocketClient(const std::string& h, const std::string& p, const std::string& u, const std::string& pem, const std::string& seed)
    :client(h, p, &session, pem, seed)
    ,session(&client, h, u){}

DC::SecureWebSocketClient::~SecureWebSocketClient()
{
    if(!session.tearDown())
    {
        _DERR("error tearing down session");
    }
}

bool DC::SecureWebSocketClient::sendPayload(const std::string& payload)
{
    const bool ret = session.sendPayload(payload);
    return ret;
}

bool DC::SecureWebSocketClient::receivePayload(std::string& payload)
{
    const bool ret = session.receivePayload(payload);
    return ret;
}

bool DC::SecureWebSocketClient::receivePayload(std::string& payload, const timespec& to)
{
    const bool ret = session.receivePayload(payload, to);
    return ret;
}
