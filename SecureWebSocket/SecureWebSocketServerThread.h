#ifndef OSEFSECUREWEBSOCKETSERVERTHREAD_H
#define OSEFSECUREWEBSOCKETSERVERTHREAD_H

#include "TLSListener.h"
#include "ThreadSpawner.h"
#include "SecureWebSocketServer.h"

namespace DC {
    
class SecureWebSocketServerThread
{
public:
    SecureWebSocketServerThread(const size_t& max
                                , const std::string& h
                                , const std::string& p
                                , WebSocketServerApplication& app
                                , const std::string& mqname
                                , const std::string& srvCert
                                , const std::string& srvKey
                                , const std::string& caCert
                                , const std::string& seed="");
    virtual ~SecureWebSocketServerThread();

    bool listen();
    bool accept(int32_t& sock);
   
private:
    SecureWebSocketServerThread(const SecureWebSocketServerThread& orig);
    SecureWebSocketServerThread& operator=(const SecureWebSocketServerThread& orig);
    
    bool serve(const int32_t& sock);
  
    static void* listenRoutine(void* const targ);
    static void* serveRoutine(void* const arg);

    OSEF::TLSListener listener;
    OSEF::ThreadSpawner threader;
    WebSocketServerApplication* application;
};

    typedef struct
    {
        SecureWebSocketServerThread* serverThread;
    }SecureWebSocketListenArg;
    
    typedef struct
    {
        int32_t sockFD;
        WebSocketServerApplication* application;
        mbedtls_ssl_config configuration;
    }SecureWebSocketServeArg;

}

#endif /* OSEFSECUREWEBSOCKETSERVERTHREAD_H */

