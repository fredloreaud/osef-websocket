#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketApplication.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketClient.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketClientSession.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketEchoApplication.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketNetBuffer.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketServerSession.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketSession.o \
	${OBJECTDIR}/_ext/b4fd4627/WebSocketUpgradeFrame.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wall -Wextra -Werror
CXXFLAGS=-Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_websocket.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_websocket.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_websocket.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_websocket.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_websocket.a

${OBJECTDIR}/_ext/b4fd4627/WebSocketApplication.o: ../../WebSocket/WebSocketApplication.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketApplication.o ../../WebSocket/WebSocketApplication.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketClient.o: ../../WebSocket/WebSocketClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketClient.o ../../WebSocket/WebSocketClient.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketClientSession.o: ../../WebSocket/WebSocketClientSession.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketClientSession.o ../../WebSocket/WebSocketClientSession.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketEchoApplication.o: ../../WebSocket/WebSocketEchoApplication.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketEchoApplication.o ../../WebSocket/WebSocketEchoApplication.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketNetBuffer.o: ../../WebSocket/WebSocketNetBuffer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketNetBuffer.o ../../WebSocket/WebSocketNetBuffer.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketServerSession.o: ../../WebSocket/WebSocketServerSession.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketServerSession.o ../../WebSocket/WebSocketServerSession.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketSession.o: ../../WebSocket/WebSocketSession.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketSession.o ../../WebSocket/WebSocketSession.cpp

${OBJECTDIR}/_ext/b4fd4627/WebSocketUpgradeFrame.o: ../../WebSocket/WebSocketUpgradeFrame.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/b4fd4627
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/osef-posix/Random -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b4fd4627/WebSocketUpgradeFrame.o ../../WebSocket/WebSocketUpgradeFrame.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
